import React, {Component} from 'react';
import {connect} from 'react-redux';
import List from 'material-ui/List';

import Restaurant from "./Restaurant";
import AddRestaurant from "./AddRestaurant";
import NewRestaurantForm from "./NewRestaurantForm";

const styles = {
  root: {
    // display: 'flex',
    // flexWrap: 'wrap',
    // justifyContent: 'space-around',
  },
  gridList: {
    width: 500,
    height: 450,
    overflowY: 'auto',
  },
};

export class RestaurantList extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
  }

  state = {
    open: true,
  };

  handleToggle = () => {
    this.setState({
      open: !this.state.open,
    });
  };


  render() {
    console.log(this.props);
    return (
      <div style={styles.root}>
        <List>
          {this.props.restaurants.restaurants.map((x) =>
            (
              <Restaurant restaurant={x}/>
            )
          )
          }
          <NewRestaurantForm/>
        </List>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    restaurants: state.restaurantReducer,
  };
}

export default connect(mapStateToProps)(RestaurantList);
