import React, {Component} from 'react';
import {connect} from 'react-redux';
import {List, ListItem} from 'material-ui/List';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {bindActionCreators} from 'redux';
import * as actions from "../../actions/index";

const style = {
  marginRight: 20,
  marginTop: 5
};

class Restaurant extends Component {
  constructor(props) {
    super(props);
  }

  addDishItem(restaurantId) {
    this.props.addDish(restaurantId);
  }


  render() {
    let nestedDishes;
    console.log(this.props.restaurant);
    console.log("xdd rest");
    if (!!this.props.restaurant.dishes) {
      nestedDishes = this.props.restaurant.dishes.map((dish) =>
        (
          <ListItem primaryText={dish.name + ": " + dish.price}></ListItem>
        ));
    } else {
      nestedDishes = <span></span>;
    }
    return (
      <ListItem primaryText={this.props.restaurant.name}
                primaryTogglesNestedList={true}
                rightIconButton={<FloatingActionButton mini={true} style={style}>
                  <ContentAdd onClick={(e) => {
                    console.log("add dish")
                    console.log(this.props.restaurant)

                    this.addDishItem(this.props.restaurant.id);
                  }}/>
                </FloatingActionButton>}
                nestedItems={nestedDishes}
      >
      </ListItem>
    );
  }
}

function mapStateToProps(state) {
  return {
    restaurants: state.restaurantReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({addDish: actions.addDish}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Restaurant);
