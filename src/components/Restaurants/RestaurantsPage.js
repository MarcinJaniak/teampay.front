import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/';
import RestaurantList from "./RestaurantList";
import {DotLoader, PulseLoader} from 'react-spinners';
import {css} from 'emotion';
import BeatLoader from "react-spinners";

const styles = {
  root: {
    // display: 'flex',
    // flexWrap: 'wrap',
    // justifyContent: 'space-around',
  },
  gridList: {
    width: 500,
    height: 450,
    overflowY: 'auto',
  },
};


class RestaurantsPage extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props);
  }

  componentDidMount() {
    console.log("mount before");

    console.log(this.props);
    //console.log(this.loadRestaurants());

    this.getRestaurantsFunc();
    console.log("mount after");

    console.log(this.props);

  }

  restaurantsPage = () => {
    this.props.actions.RestaurantsPage(this.props.restaurants);
  };


  state = {
    open: false,
  };

  loaderStyle = {
    position: 'fixed',
    top: '50%',
    left: '50%'
  };

  getRestaurantsFunc() {
    this.props.getRestaurants();
  }

  render() {
    console.log(this.props);
    if(!this.props.restaurants.loaderState)
      {
console.log("rendering list");
        return (
          <div>
            <RestaurantList/>
            <div className='sweet-loading' style={this.loaderStyle}>
              <PulseLoader
                color={'#00bcd4'}
                loading={this.props.restaurants.loaderState_post}
              />
            </div>
          </div>
        );
      } else
        {
          console.log("rendering spinner");

          return (
            <div>
              <div className='sweet-loading' style={this.loaderStyle}>
                <PulseLoader
                  color={'#00bcd4'}
                  loading={this.props.restaurants.loaderState}
                />
              </div>
            </div>
          );

        }




  }
}


function mapStateToProps(state) {
  return {
    restaurants: state.restaurantReducer
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({getRestaurants: actions.getRestaurant}, dispatch);
}

RestaurantsPage.propTypes = {
  actions: PropTypes.object.isRequired,
  restaurants: PropTypes.array.isRequired
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RestaurantsPage);
