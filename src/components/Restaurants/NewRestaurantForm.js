import React, {Component} from 'react';
import {connect} from 'react-redux';
import {List, ListItem} from 'material-ui/List';


import AddRestaurant from "./AddRestaurant";

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: 500,
    height: 450,
    overflowY: 'auto',
  },
};
const style = {
  marginLeft: 20,
};


export class NewRestaurantForm extends Component {
  constructor(props) {
    super(props);

  this.handler = this.handler.bind(this);
}

handler(e) {
  e.preventDefault();
  console.log(this.state);
this.handleToggle();
}
  state = {
    open: false, name: '', menuUrl: '',
  };


  handleToggle = () => {
    this.setState({
      open: !this.state.open,
    });
  };

  render() {
    return (
      <div style={styles.root}>
        <ListItem primaryText={"Add new restaurant"}
                  onClick={() => {
                    this.handleToggle();
                  }}>
        </ListItem>
        {this.state.open &&
        <AddRestaurant handler = {this.handler}/>
        }

      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    restaurants: state.restaurantReducer,
  };
}

export default connect(mapStateToProps)(NewRestaurantForm);
