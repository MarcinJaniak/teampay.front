import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addDish } from '../../actions';
import { bindActionCreators } from 'redux';
import {List, ListItem} from 'material-ui/List';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';

const style = {
  marginRight: 20,
};

class AddDish extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <FloatingActionButton mini={true} style={style}>
        <ContentAdd onClick={(e) => {e.preventDefault();
        this.props.dispatch(addDish())}}/>
      </FloatingActionButton>
          );
  }
}
function mapDispatchToProps(dispatch) {
  return { actions: bindActionCreators(addDish, dispatch) };
}

export default connect(mapDispatchToProps)(AddDish);
