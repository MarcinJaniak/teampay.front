import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {List, ListItem} from 'material-ui/List';
import * as actions from "../../actions/index";
import RaisedButton from 'material-ui/RaisedButton';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';

const style = {
  margin: 12,
};
const formStyle = {
  position: 'fixed',
  top: '50%',
  left: '50%'
};

class AddRestaurant extends Component {
  constructor(props) {
    super(props);

    this.state = {name: '', menuUrl: '', open: false,};
    this.handleChangeMenuUrl = this.handleChangeMenuUrl.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
  }

  handleChangeName(event) {
    this.setState({name: event.target.value});
  }

  handleChangeMenuUrl(event) {
    this.setState({menuUrl: event.target.value});
  }


  addRestaurantItem() {
    this.props.addRestaurant({name: this.state.name, url: this.state.url});
  }

  render() {
    return (
      <div>

        <Paper zDepth={2} style={formStyle}>
          <TextField value={this.state.name} onChange={this.handleChangeName} hintText="Restaurant name" style={style}
                     underlineShow={false}/>
          <Divider/>
          <TextField value={this.state.menuUrl} onChange={this.handleChangeMenuUrl} hintText="Restaurant menu url"
                     style={style} underlineShow={false}/>
          <Divider/>
          <RaisedButton label="Confirm" primary={true} style={style}

                        onClick={(e) => {
                          this.addRestaurantItem();
                          this.props.handler(e);
                        }}/>
        </Paper>
      </div>
    );
  }
}


function mapDispatchToProps(dispatch) {
  return bindActionCreators({addRestaurant: actions.addRestaurant}, dispatch);
}


// function mapDispatchToProps(dispatch) {
//   return { actions: bindActionCreators(addRestaurant, dispatch)
//   };

function mapStateToProps(state) {
  return {newRestaurant: state.newRestaurant};
}

export default connect(mapStateToProps, mapDispatchToProps)(AddRestaurant);
