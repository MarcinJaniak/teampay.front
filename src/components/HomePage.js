import React from 'react';
import { Link } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';

const HomePage = () => {
  return (
    <div>
      <h1>Team pay</h1>
      <h3>Your virtual wallet... </h3>
      <RaisedButton label="Default" />
      <h2>Get Started</h2>
      <ol>
        <li>Review the <Link to="/fuel-savings">demo app</Link></li>
        <li>Remove the demo and start coding: npm run remove-demo</li>
      </ol>
    </div>
  );
};

export default HomePage;
