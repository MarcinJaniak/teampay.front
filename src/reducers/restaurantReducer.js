import * as actionType from '../actions/ActionType';
import initialState from "./initialState";

const restaurantReducer = (state = initialState.restaurants, action) => {
  let newState;
  switch (action.type) {
    case actionType.ADD_RESTAURANT:
      return {restaurants: state.restaurants, loaderState: false, loaderState_post: true};

    case "ADD_RESTAURANT_SUCCESS":

      let restaurantToAdd = action.meta.previousAction.payload.newRestaurant;
      restaurantToAdd.id = action.payload.data.value;
      restaurantToAdd.dishes = [];
      restaurantToAdd.active = true;
      return {restaurants: state.restaurants.concat(restaurantToAdd), loaderState: false, loaderState_post: false};

    case "ADD_RESTAURANT_FAIL":
      console.error("Add restaurant fail");
      return {restaurants: state.restaurants, loaderState: false, loaderState_post: false};


    case actionType.ADD_DISH:
console.log(action.payload);
      newState = {...state};
      console.log(newState);
      console.log(state);

      let restaurant = newState.restaurants.filter(x => x.id === action.payload.restaurantId)[0];
      restaurant.dishes.push(...action.payload.dish);
      return newState;


    case actionType.GET_RESTAURANTS:
      return {
        restaurants: [...state
        ], loaderState: true
      };

    case actionType.GET_RESTAURANTS_SUCCESS:
      return {restaurants: [...action.payload.request.response], loaderState: false, loaderState_post: false};


    case
    "GET_RESTAURANTS_FAIL"
    :
      console.log("api fail");

      return {...state};

    default:
      return state;
  }
}

export default restaurantReducer;
