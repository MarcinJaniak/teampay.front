import { combineReducers } from 'redux';
import fuelSavings from './fuelSavingsReducer';
import restaurantReducer from './restaurantReducer';
import { routerReducer } from 'react-router-redux';

const rootReducer = combineReducers({
  fuelSavings,
  restaurantReducer,
  routing: routerReducer
});

export default rootReducer;

