export default {
  loaderState:true,
  loaderState_post:false,
  restaurants: [
    {
      img: 'images/grid-list/00-52-29-429_640.jpg',
      name: 'Hawa',
      restaurantId:1,
      author: 'jill111',
      dishes: [
        {
          name: "Pizza",
          price: 15
        },
        {
          name: "Pizza2",
          price: 13
        }
      ]
    }, {
      img: 'images/grid-list/00-52-29-429_640.jpg',
      name: 'Mac',
      restaurantId:2,
      author: 'jill111',
      dishes: [
        {
          name: "Br",
          price: 15
        },
        {
          name: "Fr",
          price: 15
        }
      ]
    },
  ],


  fuelSavings: {
    newMpg: '',
    tradeMpg: '',
    newPpg: '',
    tradePpg: '',
    milesDriven: '',
    milesDrivenTimeframe: 'week',
    displayResults: false,
    dateModified: null,
    necessaryDataIsProvidedToCalculateSavings: false,
    savings: {
      monthly: 0,
      annual: 0,
      threeYear: 0
    }
  }
};
