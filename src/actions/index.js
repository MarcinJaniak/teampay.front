import * as actionType from './ActionType';
import {AxiosRequestConfig, AxiosResponse} from 'axios';
import Service from '../axios/axiosService';
import {dispatch} from "redux";

export const addRestaurant = (newRestaurant) => ({
  type: actionType.ADD_RESTAURANT,
  payload: {
    request: {
      method: 'post',
      url: 'http://localhost:57697/Restaurants/CreateNewRestaurant/' + newRestaurant.name + '/' + newRestaurant.url,
    },
    newRestaurant: newRestaurant
  }
});

export const getRestaurant = () => ({
  type: actionType.GET_RESTAURANTS,
  payload: {
    request: {
      url: 'http://localhost:57697/Restaurants'
    }

  }
});


export const addDish = (restaurantId) => ({
  type: actionType.ADD_DISH,
  payload: {
    restaurantId,
    dish: [{
      name: "ewq",
      price: 15
    }]
  }
});
